import 'package:flutter/material.dart';
import 'package:medpass/widgets/MPAppBarWidget.dart';
import 'package:medpass/widgets/Chat/MPChatsWidget.dart';
import 'package:medpass/widgets/MPStartCardWidget.dart';
import 'package:medpass/widgets/Chat/MPStartNewChatWidget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(100), 
            child: MPAppBarWidget()),
        body: SingleChildScrollView(
          child: Column(
            children: [
                MPStartNewChatWidget(),
                SizedBox(height: 15),
                MPChatsWidget(),
                MPStartCardWidget()
            ],
          ),
        ),
      ),
    );
  }
}


