import 'package:flutter/material.dart';

class MPUI {
  BoxDecoration addBorder(Color color, double radius) {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        border: Border.all(color: color));
  }

  
}
