import 'package:flutter/material.dart';
import 'package:medpass/widgets/MPConfirmButtonWidget.dart';

class MPStartCardDescriptionWidget extends StatelessWidget {
  const MPStartCardDescriptionWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            '5 ways to reduce my weight',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w600),
          ),
          const SizedBox(
            height: 10,
          ),
          const Text(
            'Tap \"Start\" to being',
            style: TextStyle(fontSize: 9),
            textAlign: TextAlign.left,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
              width: 100,
              child: const MPConfirmButtonWidget(
                title: 'START',
                img: 'arrow_right.png',
              ))
        ],
      ),
    );
  }
}