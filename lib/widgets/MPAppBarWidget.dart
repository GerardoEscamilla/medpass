import 'package:flutter/material.dart';
import 'package:medpass/UI/MPUITools.dart';

class MPAppBarWidget extends StatelessWidget {
  const MPAppBarWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Container(
        alignment: Alignment.centerLeft,
        child: const Text(
          'AI-Powerred chat',
          style: TextStyle(fontFamily: 'Carena', color: Color(0xFF2f076a)),
          textAlign: TextAlign.left,
        ),
      ),
      leading: Container(
        margin: EdgeInsets.only(top: 10, left: 10),
        decoration: MPUI().addBorder(Colors.pink, 5.0),
        child: RotatedBox(
          quarterTurns: 2,
          child: IconButton(
            icon: Image.asset(
              'assets/icons/arrow_right.png',
              color: Colors.pink,
            ),
            onPressed: () {},
          ),
        ),
      ),
      leadingWidth: 50,
    );
  }
}

