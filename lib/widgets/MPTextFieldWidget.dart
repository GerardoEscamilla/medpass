import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MPTextFieldWidget extends StatelessWidget {
  const MPTextFieldWidget({
    super.key,
    required this.hint,
    required this.icon,
    required this.label,
    required this.textAlign,
  });
  final String hint;
  final String icon;
  final String label;
  final TextAlign textAlign;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [
        SizedBox(
          child: Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text(label)),
          width: double.infinity,
        ),
        TextField(
            textAlign: textAlign,
            decoration: InputDecoration(
              hintText: hint,
              border: OutlineInputBorder(),
              suffixIconConstraints:
                  BoxConstraints(maxHeight: 30, minWidth: 30),
              suffixIcon: Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: Image.asset(
                  'assets/icons/$icon',
                ),
              ),
            )),
      ]),
    );
  }
}
