import 'package:flutter/material.dart';

class MPTitleSeccionWidget extends StatelessWidget {
  const MPTitleSeccionWidget({
    super.key
  });

  @override
  Widget build(BuildContext context) {

  const textStyleSubtitle = TextStyle(
                  fontSize: 18, fontFamily: 'Carena', color: Color(0xFF2f076a));
    return Container(
      child: const Column(
        children: [
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Quick strart',
                style: textStyleSubtitle,
                textAlign: TextAlign.left,
              )),
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Here\'re a few ideas to help you  start',
                style: TextStyle(fontSize: 11, color: Color(0xFFaeaaaa)),
              ))
        ],
      ),
    );
  }
}
