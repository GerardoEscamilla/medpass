import 'package:flutter/material.dart';
import 'package:medpass/widgets/MPStartCardContainerWidget.dart';
import 'package:medpass/widgets/MPTitleSeccionWidget.dart';

class MPStartCardWidget extends StatelessWidget {
  const MPStartCardWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: const Column(
        children: [
          MPTitleSeccionWidget(),
          SizedBox(
            height: 10,
          ),
          MPStartCardContainerWidget(),
        ],
      ),
    );
  }
}


