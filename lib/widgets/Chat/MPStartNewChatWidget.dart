import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:medpass/widgets/MPConfirmButtonWidget.dart';

class MPStartNewChatWidget extends StatelessWidget {
  const MPStartNewChatWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    const textStyle = TextStyle(
      fontSize: 28,
      fontFamily: 'Carena',
      color: Color(0xFF2f076a),
    );
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Explore nutrition chat',
            style: textStyle,
            textAlign: TextAlign.left,
          ),
          const Text(
            'Meet our newest geature: personalized dietary advice at your finguertips',
            style: TextStyle(fontSize: 16, color: Color(0xFF343434)),
          ),
          const SizedBox(
            height: 15,
          ),
          GestureDetector(
            child: const MPConfirmButtonWidget(
              title: 'START A NEW CHAT',
              img: 'message_bubbles.png',
            ),
            onTap: () {
              Navigator.pushNamed(context, 'register');
            },
          )
        ],
      ),
    );
  }
}
