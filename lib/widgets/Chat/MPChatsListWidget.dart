import 'package:flutter/material.dart';
import 'package:medpass/Models/MPItemsDemo.dart';
import 'package:medpass/widgets/Chat/MPChatItemWidget.dart';

class MPChatListWidget extends StatelessWidget {
  const MPChatListWidget({
    super.key,
    required this.list,
  });

  final List<ItemsDemo> list;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.horizontal,
      itemCount: list.length,
      separatorBuilder: (BuildContext context, int index) {
        return const SizedBox(width: 30);
      },
      itemBuilder: (BuildContext context, int index) {
        return MPChatItemList(list: list, item: list[index]);
      },
    );
  }
}

