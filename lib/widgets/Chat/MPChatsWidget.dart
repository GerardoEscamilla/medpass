import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:medpass/Models/MPItemsDemo.dart';
import 'package:medpass/widgets/Chat/MPChatsListWidget.dart';

class MPChatsWidget extends StatelessWidget {
  const MPChatsWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final list = [
      ItemsDemo('5 Ideas to reduce my weight', 'assets/icons/dumbbell.png'),
      ItemsDemo(
          'Easy Pasta recipes with few ingredients', 'assets/icons/apple.png')
    ];

    const textStyleSubtitle = TextStyle(
                  fontSize: 18, fontFamily: 'Carena', color: Color(0xFF2f076a));
    return Container(
      padding: const EdgeInsets.all(18),
      alignment: Alignment.centerLeft,
      width: double.infinity,
      height: 180,
      child: Column(
        children: [
          const Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Recent Chats',
              style: textStyleSubtitle,
            ),
          ),
          const Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'See your recent conversations',
              style: TextStyle(fontSize: 11, color: Color(0xFFaeaaaa)),
            ),
          ),
          Expanded(
              child: MPChatListWidget(list: list))
        ],
      ),
    );
  }
}
