import 'package:flutter/material.dart';
import 'package:medpass/Models/MPItemsDemo.dart';
import 'package:medpass/UI/MPUITools.dart';

class MPChatItemList extends StatelessWidget {
  const MPChatItemList({
    super.key,
    required this.list, required this.item,
  });

  final List<ItemsDemo> list;
    final ItemsDemo item;
    
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      width: 180,
      height: 150,
      color: Colors.white,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  padding: EdgeInsets.all(5),
                  decoration:
                      MPUI().addBorder(Colors.deepPurpleAccent, 5.0),
                  child: Image.asset(
                    item.img,
                    width: 20,
                    height: 20,
                    fit: BoxFit.scaleDown,
                  ),
                )),
            const SizedBox(
              height: 5,
            ),
            Text(
              item.text,
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: const Color.fromARGB(255, 57, 30, 105)),
            )
          ]),
    );
  }
}
