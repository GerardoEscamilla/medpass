import 'package:flutter/material.dart';
import 'package:medpass/widgets/MPStartCardDescriptionWidget.dart';

class MPStartCardContainerWidget extends StatelessWidget {
  const MPStartCardContainerWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      height: 150,
      child: Stack(
        children: [
          Positioned(
            top: 0,
            right: 1,
            child: Image.asset(
              'assets/icons/thinking_man.png',
              fit: BoxFit.contain,
              width: 200,
            ),
          ),
          const MPStartCardDescriptionWidget(),
        ],
      ),
    );
  }
}
