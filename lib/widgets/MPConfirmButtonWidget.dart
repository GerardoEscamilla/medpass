import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MPConfirmButtonWidget extends StatelessWidget {
  const MPConfirmButtonWidget({
    super.key,
    required this.title,
    required this.img,
  });

  final bool isEnable = true;
  final String title;
  final String img;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 40,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: isEnable ? Color(0xFFee3e92) : Colors.pink[200]),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              title,
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(
              width: 10,
            ),
            Container(
                width: 20,
                height: 20,
                child: img.isNotEmpty
                    ? ImageIcon(AssetImage('assets/icons/$img'),
                        color: Colors.white)
                    : Container())
          ]),
    );
  }
}
