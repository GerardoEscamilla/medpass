import 'package:flutter/material.dart';
import 'package:medpass/register/Widgets/MPRegisterFormWidget.dart';
import 'package:medpass/register/Widgets/MPRegisterHeaderWidget.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    
    final screenSize = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[12],
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              children: [
                const MPRegisterHeaderWidget(),
                MPRegisterFormWidget(screenSize: screenSize),
              ],
            ),
          ),
        ),
      ),
    );
  }
}



