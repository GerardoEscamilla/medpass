import 'package:flutter/material.dart';
import 'package:medpass/widgets/MPConfirmButtonWidget.dart';

class MPRegisterFooterWidget extends StatelessWidget {
  const MPRegisterFooterWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text('Information is being collected for medical records'),
          const SizedBox(
            height: 5,
          ),
          GestureDetector(
            child: MPConfirmButtonWidget(
              title: 'CONTINUE',
              img: '',
            ),
            onTap: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
    );
  }
}
