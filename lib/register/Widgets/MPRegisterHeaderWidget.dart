import 'package:flutter/material.dart';

class MPRegisterHeaderWidget extends StatelessWidget {
  const MPRegisterHeaderWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    const boxDecoration = BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
        gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [Color(0xff2e305f), Color(0xff202333)]));
    return Stack(
      children: [
        Container(
          width: double.infinity - 30,
          height: 100,
          decoration: boxDecoration,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(20),
              child: Container(
                  width: 50,
                  height: 50,
                  child: Image.asset('assets/icons/medpass_logo.png')),
            ),
            
               Text(
                "Complete your profile.",
                style: TextStyle(color: Colors.white),
              ),
            
          ],
        )
      ],
    );
  }
}
