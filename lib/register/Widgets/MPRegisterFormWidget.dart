import 'package:flutter/material.dart';
import 'package:medpass/register/Widgets/MPRegisterFooterWidget.dart';
import 'package:medpass/widgets/MPTextFieldWidget.dart';

class MPRegisterFormWidget extends StatelessWidget {
  const MPRegisterFormWidget({
    super.key,
    required this.screenSize,
  });

  final Size screenSize;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          MPTextFieldWidget(
            hint: 'Enter your name',
            icon: 'user.png',
            label: 'Name',
            textAlign: TextAlign.left,
          ),
          const SizedBox(
            height: 20,
          ),
          const MPTextFieldWidget(
            hint: 'mm/dd/yyyy',
            icon: 'calendar.png',
            label: 'Date of Birth',
            textAlign: TextAlign.left,
          ),
          const SizedBox(
            height: 20,
          ),
          const MPTextFieldWidget(
            hint: 'lbs',
            icon: 'weightmeter.png',
            label: 'Weight',
            textAlign: TextAlign.right,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: screenSize.width - 30,
            child: const Row(
              children: [
                Flexible(
                  child: MPTextFieldWidget(
                    hint: 'Feet',
                    icon: 'ruler.png',
                    label: 'Height',
                    textAlign: TextAlign.right,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Flexible(
                  child: MPTextFieldWidget(
                    hint: 'Inchs',
                    icon: 'ruler.png',
                    label: '',
                    textAlign: TextAlign.right,
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: const MPTextFieldWidget(
              hint: 'Gender',
              icon: 'gender.png',
              label: '',
              textAlign: TextAlign.left,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
        MPRegisterFooterWidget()
        ],
      ),
    );
  }
}

