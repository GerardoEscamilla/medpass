import 'package:flutter/material.dart';
import 'package:medpass/home/HomeScreen.dart';
import 'package:medpass/register/RegisterScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: 'home',
      theme: ThemeData.light().copyWith(
        appBarTheme: AppBarTheme(elevation: 0, color: Colors.white),
        scaffoldBackgroundColor: Color.fromRGBO(255, 255, 255, 0.97),
        colorScheme: ColorScheme.fromSeed(
            seedColor: Colors.deepPurple,
            primary: Color(0xFF2f076a),
            secondary: Color(0xFF343434),
            tertiary: Color(0xFFaeaaaa),
            background: Color.fromRGBO(255, 255, 255, 0.9)),
      ),
      routes: {
        'register': (_) => const RegisterScreen(),
        'home': (_) => const HomeScreen(),
      },
    );
  }
}
